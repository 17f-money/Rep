---
title: Future/Forecasting Markets
date: 2017-11-05
tags: ["Future Markets"]
---

In future markets pricing is based on the perceived likelihood of an event occurring. If something is deemed more likely to happen the price increases, and as something becomes increasing unlikely to occur the price decreases.  In an example event with two outcomes, the prices of each would add to a dollar. Take a football game for example, with people placing money on each team.  If the team's are relatively equal at the start of the game it is likely that the pricing for each will be around 50 cents. However, if for some reason one team is up by 35 at half-time, then the price of the outcome for that team winning will be much higher.  Shares of each outcome would continue to trade until the football game is over, at which point everyone who owns shares in the correct outcome would recieve a dollar for each share and everyone who owns the incorrect outcome would lose the value of their share. This idea works for events with multiple outcomes (as long as they are finite) and can be applied to other events such as the expected future prices of commodities or the results of elections. 
