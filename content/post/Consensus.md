---
title: Augur's Consensus
date: 2017-11-05
tags: ["Consensus", "Reporting"]
bigimg: [{src: "/img/Consensus Path.jpg", desc: "Diagram of Reporting Consensus in Augur"}]
---

Augur uses a unique consensus method in order to determine the correct outcome of events based on how it's reporters report the outcomes.  The path it follows through consensus is shown in the diagram above.

First is the Pre-Reporting/Active Trading period in which the market experiences trades of different outcomes.  After the event closes the Designated Reporting Period begins; this is a 3 day period in which one person (designated by the Market Creator) reports the outcome of the event.  If no one disputes the event results are finalized at this time.  However, the event could enter the Designated Dispute period if someone disagrees with the decision made by the designated reporter.  In order to voice their disagreement they must post a "dispute bond," currently set at 110 REP.  Then there is a maximum of a 30 day waiting period as the event prepares to enter the Limited reporting stage.  This is a 27 day period in which REP holders can report on the outcome of the event. If no one disputes the agreed upon outcome, the event is finalize.  If someone posts a dispute bond (1100 REP in this case), then the event moves to the All Reporting stage.  This is the same as the Limited Reporting Stage except that ALL REP holders are required to select the correct outcome.  Once again, there is a dispute option, although this time the dispute bond is set at 110000 REP.  If a dispute bond is met after the All Reporting stage, it leads to a fork and two "alternate universes" where people can still use REP depending on the outcome that they believe in.  