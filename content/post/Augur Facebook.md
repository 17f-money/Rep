---
title: Augur's Use of Facebook
date: 2017-11-08
tags: ["Augur", "Social Media"]
---

It is fascinating to observe how Augur has grown without much help from Facebook. It is interesting to see that Augur, despite 16,230 page likes, Augur rarely surpasses 10 likes on any given post.  So clearly, facebook is not really a necessary force to use in terms of advancing a cryptocurrency, likely because it is outdated and has an older user-base than other platforms.  It is really funny to look at both Augur's first post and most recent post.  It's first post was simply an updated cover photo with a screenshot from the Augur Alpha platform.  It recieved 3 likes, one from a founder (Matt Liston), one from the page itself, and one from Joey Krug's Mom.  Joey Krug is the other founder of Augur. Although the page has stopped liking it's own posts, Augur recieved just 5 likes on it's most recent post, one of which was from the ever supportive mom of Joey Krug.  Clearly, facebook is not the platform to use in attempting to advance a crowdsale, but it seems necessary to use because the page likely gives it credibility even without the page likes.