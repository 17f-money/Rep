---
title: Reputation Crowdsale
date: 2017-11-05
tags: ["crowdsale", "Augur"]
---

In 2015 Augur organized an ICO with it's ERC20 Token, Reputation, in order to raise money to support the project.  This crowdsale of REP tokens lasted 45 days, from August 17th, 2015 through October 1st, 2015.  It raised over $5.2 million worth of Bitcoin and Ethereum through it's crowdsale, making it one of the most successful crowdsales of all time.  