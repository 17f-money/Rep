---
title: What Is Augur?
date: 2017-11-05
tags: ["Augur", "Future Market"]
---

Augur is an open-sourced prediction market platform.  Essentially, Augur is trying to create a more efficient, decentralized platform for trading future positions (see previous post for more information about future markets).  Augur's main goal is to create an accurate forecasting tool for events by using the "wisdom of the crowd."  The way this works is that someone creates a market for a certain event.  This event is then opened to the public and people can purchase shares on different outcomes.  Since Augur is based on the blockchain, and the blockchain has no way of checking if an event happened in the check for an outcome is represented as the result decided on by Augur's Reporters (See Consensus section for more on this). For more information on the specifics feel free to visit Augur's website at https://augur.net/.  