---
title: Reputation Token
#subtitle: Best sport ever!
date: 2015-01-19
tags: ["Augur", "REP","ERC 20"]
---

Augur's Reputation Token (REP) currently has a market cap of $181 million.  There are 11,000,000 Rep currently in circulation and it has a 24 hours volume of $2.1 million. Considering that, as of now, Reputation cannot yet be used to report on events, this trading volume is likely the result of speculators and/or people purchasing REP before Augur goes live so that they can be a reporter.  The price of REP is incredibly volatile, hitting a high of $36 in June 2017, it is currently at $16.54, and has been hovering in the $15-20 range for the past month (October 2017).  The volatility is not surprising considering the risky nature of this coin if the Augur Platform never successfully takes off for any reason (see Problems with Augur for more).  However, it is shocking that Augur has such a high market cap considering that, at the moment, it's worth is entirely based on unpredictable future events and the hope that Augur is actually successful as a futures market. For more current stats on REPs price and market cap check out https://coinmarketcap.com/currencies/augur/.